\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
\usepackage{amsmath, amsfonts}
\usepackage{graphicx}
\graphicspath{{./figures}}

\begin{document}
\title{Research of COVID-19 Impact and Trend in USA Using Statistical Approach \\ \thanks{The author gratefully acknowledge the support from Halina.}}
\author{
	\IEEEauthorblockN{Artsiom Naslednikau}
	\IEEEauthorblockA{ORCID: 0000-0002-7630-1972}
}
\maketitle

\begin{abstract}
A study of the impact and the trend was done using public information from a ``United States COVID-19 Cases and Deaths by State over Time'' dataset and a ``U.S. Population Clock'' service for 2020. 
Purposes of the study were to validate a hypothesis that a coronavirus disease had the impact in the USA on a growth of population and to compute a possible tendency. 
The study was done by applying a discrete difference, a nth root and a mean classification operations to adjust derived subsets. 
Then, a pattern recognition, a regression and a k-means clustering methods were applied on subsets derivatives. 
A noticeable pattern was identified between cases, deaths and estimated population metrics which affirmed the hypothesized coronavirus impact on population of the United States of America. 
Both a linear regression and an isotonic-linear regression had an increasing tendency for a recovery metric and opposite tendencies for the deaths metric. 
Choropleth maps helped to discover a north-west region as the region with the lowest metrics of the country.
\end{abstract}

\begin{IEEEkeywords}
COVID-19 pandemic, pattern recognition, regression, mean-classification, k-means clustering
\end{IEEEkeywords}

\section{Introduction}
Coronavirus disease 2019 (COVID-19) is a disease caused by a severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2).
The SARS-CoV-2 infection transmission origins similar to a SARS-CoV appeared in 2003.
Reference ~\cite{Samaranayake2021} mentions that a COVID-19 pandemic has similarities to a SARS 2003 epidemic which originated in animals and a dynamics of animal-to-human transmission can be traced. 
The first case which caused a worldwide global pandemic was registered in December 2019 in Wuhan, China.
The COVID-19 pandemic is the first widespread pandemic outbreak after the Spanish Flu started in 1918 ~\cite{bell2020covid19}, which was caused by an H1N1 influenza A virus.
The COVID-19 disease infects diverse social groups of people: in ~\cite{web:statement100220} is stated that the president of the USA was diagnosed positively.
The disease affects an economy: in ~\cite{tang2020functional} is written that an unemployment rate of the U.S. in April 2020 had a record-high over-the-month increase and reached 14.7\% since January 1948, according to the U.S. Bureau of Labor Statistics.
Various works are done on predicting of COVID-19.
Authors of ~\cite{said2020predicting} calculate predictions of daily cumulative cases using Bidirectional LSTM.
Authors of ~\cite{Kumar_2020} make out-of-sample predictions for confirmed infection cases using ARIMA and FBProphet.
\par A hypothesized result of the project is a proof of the coronavirus disease impact on population dynamics in the USA.
The growth of population is expected to decrease in time intervals when a number of deaths from the disease increases.
In addition to the impact, the other goal of the research is the trend identification for the recovery and the deaths metrics using regression models.
An investigation method is based on an approach which intersects with machine learning, data mining and statistical techniques.
A working dataset is formed using the ``United States COVID-19 Cases and Deaths by State over Time'' dataset ~\cite{web:covid-dataset} which is merged with estimated population from the ``U.S. Population Clock'' service ~\cite{web:population-clock}.
Subset derivatives for a country and states are taken from the working dataset using a grouping technique.
The analysis is done in four major steps.
\begin{enumerate}[\IEEEsetlabelwidth{12)}]
	\item The discrete backwards difference and the $n$th root operations are applied for the country subset. Death line charts with the cases and the population metrics are built.
	\item The linear and the isotonic-linear regressions are constructed for the country subset.
	\item The mean-classification is performed for the states subset. Choropleth maps for the recovery and the deaths metrics are made.
	\item The unsupervised $k$-means clustering is done for the states subset.
\end{enumerate}

\section{Methods}
\subsection{Subset derivatives}
Information materials were aggregated from public sources and merged into the one dataset.
Coronavirus materials were taken from the dataset shared by CDC (The Centers for Disease Control and Prevention) and information of estimated U.S. population was provided by The United States Census Bureau.
Estimated population computation was described in ~\cite{misc:population-method} using demographic balancing
\begin{equation}
	p' = p + b - d + m
\end{equation}
where $p'$ was a metric of estimated population, $p$ was a metric of base population, $b$ was a metric of births, $d$ was a metric of death and $m$ was a metric of migration.
Table \ref{tab:the_coronavirus_dataset_description} described in total 15 distinct metrics of the dataset from CDC.
\begin{table}[tb]
	\begin{center}
	\caption{The CDC dataset columns description}
	\label{tab:the_coronavirus_dataset_description}
	\begin{tabular}{|l|l|l|}
		\hline
		Property & Description & Type \\ 
		\hline
		submission\_date & Date of counts & Date \& Time \\
		state & Jurisdiction & Plain Text \\
		tot\_cases & Total number of cases & Number \\
		conf\_cases & Total confirmed cases & Number \\
		prob\_cases & Total probable cases & Number \\
		new\_case & Number of new cases & Number \\
		pnew\_case & Number of new probable cases & Number \\
		tot\_death & Total number of deaths & Number \\
		conf\_death & Total number of confirmed deaths & Number \\
		prob\_death & Total number of probable deaths & Number \\
		new\_death & Number of new deaths & Number \\
		pnew\_death & Number of new probable deaths & Number \\
		created\_at & Date and time record was created & Date \& Time \\
		consent\_cases & Composite field & Plain Text \\
		consent\_deaths & Composite field & Plain Text \\
		\hline
	\end{tabular}
	\end{center}
\end{table}
During an analysis of the dataset from CDC missing values were found indicating inconsistency of the collected metrics.
For an accuracy purpose only confirmed metrics were used for the research and the other metrics were eliminated from derived subsets.
\par The recovery metric was introduced to take into account development dynamics of the cases and the deaths metrics, and was used in regression models and choropleth maps.
The metric was calculated as a difference between the cases and the deaths metrics
\begin{equation}
	\label{eq:recovery}
	r = c - d
\end{equation}
where $r$ was the recovery metric, $c$ was the cases metric and $d$ was the deaths metric.
\par Two derivative subsets were created from the source dataset: the country and states subset. 
Table \ref{tab:the_country_subset_description} described 3 columns of the country subset which were grouped by date.
\begin{table}[tb]
	\begin{center}
	\caption{The country subset columns description}
	\label{tab:the_country_subset_description}
	\begin{tabular}{|l|l|l|}
		\hline
		Property & Description & Type \\
		\hline
		submission\_date & Date of counts & Date \& Time \\
		conf\_cases & Total confirmed cases & Number \\
		conf\_death & Total number of confirmed deaths & Number \\
		\hline
	\end{tabular}
	\end{center}
\end{table}
Table \ref{tab:the_states_subset_description} located in total 3 columns of the state subset which were grouped by the jurisdiction field.
\begin{table}[tb]
	\begin{center}
	\caption{The states subset columns description}
	\label{tab:the_states_subset_description}
	\begin{tabular}{|l|l|l|}
		\hline
		Property & Description & Type \\
		\hline
		state & Jurisdiction & Plain Text \\
		conf\_cases & Total confirmed cases & Number \\
		conf\_death & Total number of confirmed deaths & Number \\
		\hline
	\end{tabular}
	\end{center}
\end{table}

\subsection{The country line charts}
A time-frame adjustment was done to implement universal frequency intervals.
Only days with ordinal numbers from the set $\{1, 5, 10, 15, 20, 25\}$ were left in the country subset.
The earliest record in the subset was dated to March 2020.
The latest date left in the subset was 25th December 2020.
The CDC dataset had 1,721,928 instance records.
During the time-frame adjustment operation only 55 checkpoints were selected.
\par The discrete difference operation, meant a difference with the previous day, eliminated a cumulative growth from the country subset and was denoted as
\begin{equation}
	\label{eq:discrete_difference}
	{diff(x)} = 
	\begin{cases}
		0,&{\text{if}}\ i=0,\\
		{x_i - Bx_i,}&{\text{if}}\ i>0,
	\end{cases}
\end{equation}
where $diff$ was the discrete difference, $x$ was the country subset, $i$ was a checkpoint index and $B$ was a backshift operator was denoted as
\begin{equation}
	\label{eq:backshift}
	B: Bx_i = Bx_{i-1}
	\forall y > 1.
\end{equation}
\par The first row dated to March 25th was removed from the country subset due to a zero day restriction.
On April 1st 1575 confirmed cases were recorded and the population was estimated as 329459499 people.
On December 25th it was recorded the 7708482 cases with the estimated population equaled to 330746845 people that meant more than 2.3\% percent of the U.S. population was affected by COVID-19.
\par Values of the country subset had a big degree difference in between metrics.
The $n$th root operation eliminated the degree difference between metrics in addition to the discrete difference operation and was denoted as 
\begin{equation}
	\label{eq:nth_root}
	root(x) = {\sqrt[{n}]{x}}.
\end{equation}
where $root$ was the $n$th root operation and $n$ was a radical degree. 
Thus, the discrete difference and the $n$th root operations adjusted the country dataset values: eliminated large number powers and made possible to plot values on the same graph while dynamics of sinuous shapes were preserved.

\subsection{The country regressions}
Multiple regressions were applied to compute coronavirus tendencies: the linear and the isotonic-linear. 
Regressions were constructed using a scikit-learn machine learning library for Python. 
In the linear regression the target value was expected to be a linear combination of features which corresponded to a mathematical notation
\begin{equation}
	\label{eq:linear_regression}
	\hat{y}(w, x) = w_0 + w_1 x_1 + ... + w_p x_p
\end{equation}
where $\hat{y}$ was a predicted value, $w_0$ was an independent term and $w = (w_1,..., w_p)$ were coefficients.
The ordinary least squares linear regressions fitted a linear model to minimize the residual sum of squares between the targets of a mathematical form
\begin{equation}
	\label{eq:ordinary_least_squares}
	\min_{w} || X w - y||_2^2.
\end{equation}
The isotonic regression or a monotonic regression fitted a non-decreasing real function to 1-dimensional values which solved the problem of % minimization 
\begin{IEEEeqnarray}{Cl}
	\label{eq:isotonic_regression}
	& \min\sum_i w_i (y_i - \hat{y}_i)^2 \\
	& \textnormal{subject to}\ \hat{y}_i \le \hat{y}_j~\textnormal{whenever}~X_i \le X_j, \IEEEnonumber
\end{IEEEeqnarray}
where the weights $w_i$ were strictly positive, $X$ and $y$ were arbitrary real quantities.
The custom isotonic-linear regression was built by binding regression model: an output of linear regression was binded to an input for the isotonic regression.

\subsection{The states classification}
The derived states subset grouped by the jurisdiction field contained the 26 U.S. states: \{AL, AZ, CO, CT, DE, GA, ID, IL, KY, MA, ME, MI, MS, MT, NC, NY, OH, OK, OR, PR, SC, TN, UT, VA, WI, WY\}.
The mean classification operation detected states which had values above average for the recovery and the deaths metrics was formulated as
\begin{equation}
	\label{eq:mean_classification}
	class(x) =
	\begin{cases}
		1,&{\text{if}}\ x_i > \bar{x},\\
		{0,}&{\text{otherwise,}} \\
	\end{cases}
\end{equation}
where $class$ was the classification operation and $\bar{x}$ was mean. Mean was formulated as
\begin{equation}
	\label{eq:mean}
	\bar{x} = \frac{1}{n}\sum_{i=0}^{n} x_i
\end{equation}
where $n$ was a total number of checkpoints.
For each metric a mean value was computed, then inflated jurisdictions which exceeded a mean threshold were labeled by an over-mean flag otherwise by an under-mean flag.
\par Also, U.S. choropleth maps were built for the recovery and the deaths metrics using the plotly library for Python.
A visual analysis was done based on choropleth maps using a density metric for identification of regions with the highest and the lowest levels.

\subsection{The states k-means clustering}
The $k$-means clustering method \ref{eq:kmeans} was used to label groups in the states subset of equal variance by minimizing an inertia
\begin{equation}
	\label{eq:kmeans}
	\sum_{i=0}^{n}\min_{\mu_j \in C}(||x_i - \mu_j||^2)
\end{equation}
where $\mu_j$ were means of clusters or centroids.
The inertia or a within-cluster sum-of-squares criterion was a coherent measure of internal clusters.
The method assigned centroids, then assigned another centroids using a mean value of created centroids, then computed a difference between centroids, then assigned the other centroids when the difference was less than a threshold.
The $k$-means clustering was a method of unsupervised learning and required to define a number of disjoint clusters for building the cluster.
The exact number of clusters was selected as 2 to compare the clustering output with the states classification output for anomaly detection.
The clustering was done using the scikit-learn library.

\section{Results}
Fig. \ref{fig:adjusted_country_properties} depicted the cumulative growth of adjusted metrics such as the cases, the deaths, the recovery and the population .
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{country_dataset_adj.pdf}
	\caption{The adjusted metrics for the United States in between April to December 2020.}
	\label{fig:adjusted_country_properties}
\end{figure}
The adjusted cases metric had a noticeable increase after November.
The adjusted deaths metric had a noticeable increase in between March to May and November to December.
The adjusted population metric had a slight growth deviation in between November to December.
\par Fig. \ref{fig:discrete_country_properties} exhibited that the cumulative growth of the metrics was eliminated by applying the discrete difference operation.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{country_dataset_diff.pdf}
	\caption{The effect of the discrete difference operation on the metrics for the United States in between April to December 2020.}
	\label{fig:discrete_country_properties}
\end{figure}
Thus, the metrics began to indicate an interval growth instead of the cumulative growth: derivative shapes began to consist of more noticeable sinuous waves.
A shape of sinuous wave of the population metric was a notable example of an interval growth frequency.
The discrete difference of the cases metric decreased in between May to June and in between September to October.
Similarly, the metric had three waves of increases in the middle of April, the beginning of July and in December.
The discrete difference of the deaths metric notably increased in the middle of April and in between November to December.
The discrete difference of the population metric decreased in between April to May and November to December.
\par Fig. \ref{fig:the_country_recovery} demonstrated an effect of the $n$th root operation which reduced a degree of the recovery metric.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{country_recovery.pdf}
	\caption{The effect of the discrete difference and the nth root operations on the recovery metric for the United States in between April to December 2020.}
	\label{fig:the_country_recovery}
\end{figure}
However, a dynamics of the sinuous shape with the interval growth was preserved.
\par Fig. \ref{fig:the_country_charts} showed the cases and the population metrics to the deaths metric presented in the same curve charts.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{country_charts.pdf}
	\caption{The impact of the deaths metric to the cases and the population metrics for the United States in between April to December 2020.}
	\label{fig:the_country_charts}
\end{figure}
The discrete difference and the $n$th root operations made possible to combine and to observe shapes in shared charts with minimized difference of $x$-axis.
The cases and the deaths metrics had a common pattern: an increase of the cases and the deaths metrics in the middle of April.
Also, another pattern occurred in between October and December: a large increase of the cases metric and a less noticeable increase of the deaths metric.
The deaths metric and the population metric had noticeable patterns in the middle of April and in between October to December which were characterized by a decrease of the population and an increase of the deaths.
Taking into consideration that the time intervals coincided on both charts and the cases metric had the more noticeable increase in between October and December it was concluded that the biggest impact was at the end of the year.
\par Fig. \ref{fig:the_country_regressions} compared predicting tendencies of the linear and the isotonic-linear regressions.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{country_regressions.pdf}
	\caption{Regressive trends of the recovery and the deaths metrics for the United States in between April December 2020.}
	\label{fig:the_country_regressions}
\end{figure}
The recovery metric had almost an equal prediction tendency for both regressions with a little deviation.
Both regression models strove to increase a significant slope angle for the recovery metric. 
The regressive trends for the deaths metric were opposite but had minor slope angles.
The linear regression had a tendency to decrease and the isotonic-linear regression had a tendency to increase.
\par Table \ref{tab:the_states_subset} presented the state subset classification results for the recovery and the deaths metrics.
The label of ``1'' meant that a U.S. state was labeled as over-mean for a certain metric and contained above-average values.
Most states were labeled equally for the recovery and the deaths metrics.
However, the states of Colorado and Wisconsin were classified as over-mean for the recovery metric and under-mean for the deaths metric.
In total 9 states from 26 were labeled as over-mean for the recovery and the death metrics: {AZ, GA, IL, MA, MI, NC, NY, OH, TN}.
\begin{table}[tb]
	\begin{center}
	\caption{The states subset}
	\label{tab:the_states_subset}
	\begin{tabular}{|c|r|r|r|c|c|}
		\hline
		U.S.     & Confirmed & Confirmed & Recovery & Deaths & Recovery \\
		State & Cases & Deaths & & Class & Class \\
		\hline
		AL & 277754 & 4096 & 273658 & 0 & 0 \\
		AZ & 465013 & 7635 & 457378 & 1 & 1 \\
		CO & 308907 & 3948 & 304959 & 0 & 1 \\
		CT & 162449 & 4686 & 157763 & 0 & 0 \\
		DE & 51486 & 791 & 50695 & 0 & 0 \\
		GA & 537079 & 9656 & 527423 & 1 & 1 \\
		ID & 112397 & 1191 & 111206 & 0 & 0 \\
		IL & 930849 & 15799 & 915050 & 1 & 1 \\
		KY & 202044 & 2312 & 199732 & 0 & 0 \\
		MA & 328307 & 11706 & 316601 & 1 & 1 \\
		ME & 18517 & 314 & 18203 & 0 & 0 \\
		MI & 469928 & 11775 & 458153 & 1 & 1 \\
		MS & 143736 & 3718 & 140018 & 0 & 0 \\
		MT & 78929 & 916 & 78013 & 0 & 0 \\
		NC & 467480 & 6152 & 461328 & 1 & 1 \\
		NY & 365473 & 20151 & 345322 & 1 & 1 \\
		OH & 590630 & 7687 & 582943 & 1 & 1 \\
		OK & 229462 & 2218 & 227244 & 0 & 0 \\
		OR & 105998 & 1407 & 104591 & 0 & 0 \\
		PR & 68461 & 1187 & 67274 & 0 & 0 \\
		SC & 263392 & 4662 & 258730 & 0 & 0 \\
		TN & 485728 & 5646 & 480082 & 1 & 1 \\
		UT & 260589 & 1182 & 259407 & 0 & 0 \\
		VA & 279153 & 4272 & 274881 & 0 & 0 \\
		WI & 467899 & 4679 & 463220 & 0 & 1 \\
		WY & 36700 & 373 & 36327 & 0 & 0 \\
		\hline
	\end{tabular}
	\end{center}
\end{table}
Fig. \ref{fig:states_deaths} showed a choropleth map of the deaths metric for the states subset.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{states_deaths.pdf}
	\caption{The choropleth map of the deaths metric for the U.S. states in between April to December 2020.}
	\label{fig:states_deaths}
\end{figure}
The state of New York located in the north-east of the map had the highest density of the deaths metric compared to the other states.
States located in the north-west region had the noticeable lowest density of the deaths metric.
\par Fig. \ref{fig:states_recovery} depicted a choropleth map of the recovery metric for the states subset.
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{states_recovery.pdf}
	\caption{The choropleth map of the recovery metric for the U.S. states in between April to December 2020.}
	\label{fig:states_recovery}
\end{figure}
The state of Illinois located in the middle of the map had the highest density of the recovery metric compared to the other states.
The north-west region had the lowest recovery metric density.
\par Fig. \ref{fig:states_cluster} presented the states subset partitioned into two disjoint clusters based on the cases and the deaths metrics. 
\begin{figure}[tb]
	\centering
	\includegraphics[width=240px, keepaspectratio]{states_cluster.pdf}
	\caption{The $k$-means disjoint clusters of the cases and the deaths metrics for the U.S. states in between April to December 2020.}
	\label{fig:states_cluster}
\end{figure}
Both disjoint clusters or partitions were built using the $k$-means method and showed separation between big and small values.
The states of Colorado and Massachusetts were labeled as over-mean for the recovery metric but were assigned to the partition with small values.
Moreover, the state of Massachusetts was also labeled as over-mean for the deaths metric.
The state of Wisconsin was assigned in the partition with big values and was labeled as under-mean for the deaths metric.

\section{Discussion}
In this paper, problems of identifying the trend and the impact were studied for coronavirus disease based on publicly available information.
Multiple methods from computer science and statistics fields were used to investigate the problems such as the curve chart, the regression analysis, the classification, the choropleth map and the $k$-means clustering.
The execution results of methods were compared between each other, analyzed and documented.
The calculations were done based on estimated values which affected result accuracies and limited identification of notable deviations.
\par Nevertheless, the author was able to notice the impact of the coronavirus disease on the estimated population of the United States of America.
The noticeable pattern was identified between the cases, the deaths and the population metrics on the line charts.
It was concluded that the end of the year was a pandemic peak of in 2020.
Also, it was found that the recovery growth, which was the difference between the cases and the deaths metrics, had the increasing trend based on both regression models.
In case of the deaths, metric regression models had opposite tendencies: the linear regression had the tendency to decrease and the isotonic-linear regression had the tendency to increase.
The choropleth maps helped compare U.S. states and to find the region with the lowest level metrics located in the north-west of the country.
\par A future research direction might be applications of an auto-regressive integrated moving average (ARIMA) model and an artificial neural network (ANN) such as a long short-term memory (LSTM) to predict out-of-sample confidence intervals.
Besides, a usage of another data source might improve accuracy of methods and lead to another set of discoveries.

\section*{Acknowledgment}
The first version of the working paper was written in 2020 as a supplemental material for a presentation on the 2nd Machine Intelligence Day conference hosted by Seidenberg School of Computer Science and Information Systems at Pace University, New York, New York. In 2023 the paper was significantly revised and updated for a pre-printing purpose.

\bibliography{covid-impact}

\newpage
\appendix
\begin{table}[ht]
	\begin{center}
	\caption{The country subset}
	\label{tab:the_country_subset}
	\begin{tabular}{|c|r|r|r|c|}
		\hline
		Date & Confirmed & Confirmed & Recovery & Population \\
		& Cases & Deaths &  & \\
		\hline
		03/25/2020 & 504 & 1 & 503 & 329433166 \\
		04/01/2020 & 1584 & 9 & 1575 & 329459499 \\
		04/05/2020 & 2585 & 31 & 2554 & 329476690 \\
		04/10/2020 & 3908 & 74 & 3834 & 329498178 \\
		04/15/2020 & 130619 & 7672 & 122947 & 329519667 \\
		04/20/2020 & 326443 & 18652 & 307791 & 329541155 \\
		04/25/2020 & 436615 & 25705 & 410910 & 329562644 \\
		05/01/2020 & 515576 & 31842 & 483734 & 329588430 \\
		05/05/2020 & 561470 & 34182 & 527288 & 329606219 \\
		05/10/2020 & 625946 & 38446 & 587500 & 329628455 \\
		05/15/2020 & 680173 & 42034 & 638139 & 329650692 \\
		05/20/2020 & 732398 & 44858 & 687540 & 329672928 \\
		05/25/2020 & 784045 & 47415 & 736630 & 329695164 \\
		06/01/2020 & 849940 & 50655 & 799285 & 329726295 \\
		06/05/2020 & 886338 & 52305 & 834033 & 329746456 \\
		06/10/2020 & 929645 & 54055 & 875590 & 329771658 \\
		06/15/2020 & 975503 & 55495 & 920008 & 329796860 \\
		06/20/2020 & 1038759 & 57050 & 981709 & 329822061 \\
		06/25/2020 & 1104655 & 58432 & 1046223 & 329847263 \\
		07/01/2020 & 1205765 & 60491 & 1145274 & 329877505 \\
		07/05/2020 & 1284999 & 61192 & 1223807 & 329899443 \\
		07/10/2020 & 1388488 & 62810 & 1325678 & 329926866 \\
		07/15/2020 & 1503448 & 64192 & 1439256 & 329954289 \\
		07/20/2020 & 1626670 & 65743 & 1560927 & 329981711 \\
		07/25/2020 & 1756264 & 68034 & 1688230 & 330009134 \\
		08/01/2020 & 1925549 & 70771 & 1854778 & 330047526 \\
		08/05/2020 & 2000485 & 72072 & 1928413 & 330069263 \\
		08/10/2020 & 2102576 & 73367 & 2029209 & 330096434 \\
		08/15/2020 & 2197800 & 75444 & 2122356 & 330123605 \\
		08/20/2020 & 2276011 & 77204 & 2198807 & 330150776 \\
		08/25/2020 & 2358032 & 78780 & 2279252 & 330177947 \\
		09/01/2020 & 2475466 & 81236 & 2394230 & 330215986 \\
		09/05/2020 & 2550763 & 82689 & 2468074 & 330238125 \\
		09/10/2020 & 2616739 & 83918 & 2532821 & 330265798 \\
		09/15/2020 & 2699039 & 85415 & 2613624 & 330293471 \\
		09/20/2020 & 2792140 & 86785 & 2705355 & 330321145 \\
		09/25/2020 & 2879397 & 88414 & 2790983 & 330348818 \\
		10/01/2020 & 2988233 & 90009 & 2898224 & 330382026 \\
		10/05/2020 & 3066371 & 90909 & 2975462 & 330400989 \\
		10/10/2020 & 3182475 & 92432 & 3090043 & 330424693 \\
		10/15/2020 & 3305166 & 93699 & 3211467 & 330448397 \\
		10/20/2020 & 3440605 & 95094 & 3345511 & 330472101 \\
		10/25/2020 & 3606521 & 96895 & 3509626 & 330495805 \\
		11/01/2020 & 3867726 & 99417 & 3768309 & 330528990 \\
		11/05/2020 & 4052847 & 101196 & 3951651 & 330546051 \\
		11/10/2020 & 4346956 & 103238 & 4243718 & 330567378 \\
		11/15/2020 & 4694410 & 105859 & 4588551 & 330588705 \\
		11/20/2020 & 5075047 & 109352 & 4965695 & 330610031 \\
		11/25/2020 & 5420314 & 112452 & 5307862 & 330631358 \\
		12/01/2020 & 5834653 & 115881 & 5718772 & 330656950 \\
		12/05/2020 & 6170432 & 119987 & 6050445 & 330671932 \\
		12/10/2020 & 6574954 & 124411 & 6450543 & 330690661 \\
		12/15/2020 & 6997693 & 128753 & 6868940 & 330709389 \\
		12/20/2020 & 7386553 & 133790 & 7252763 & 330728117 \\
		12/25/2020 & 7708482 & 138161 & 7570321 & 330746845 \\
		\hline
		\end{tabular}
		\end{center}
\end{table}
\end{document}
