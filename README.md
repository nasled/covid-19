# covid-impact

Research of COVID-19 Impact and Trend in USA Using Statistical Approach.

The working paper was written as a supplemental material for the 2nd Machine Intelligence Day conference hosted by Seidenberg School of Computer Science and Information Systems.

The repository contains source codes, reference papers, a presentation file and the other information from [Centers for Disease Control and Prevention](https://cdc.gov/) and [US Census Bureau](https://census.gov/).

## Paper Build
To build a working paper
```
make
```
