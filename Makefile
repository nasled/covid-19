MAIN_FILE=covid-impact
BIB_FILE=covid-impact
ARX_FILE=ax.tar

file-nonstop:
	pdflatex -synctex=1 -interaction=nonstopmode $(MAIN_FILE).tex
	pdflatex -synctex=1 -interaction=nonstopmode $(MAIN_FILE).tex

file-nonstop-bibtex:
	pdflatex -synctex=1 -interaction=nonstopmode $(MAIN_FILE).tex
	bibtex $(BIB_FILE)
	pdflatex -synctex=1 -interaction=nonstopmode $(MAIN_FILE).tex
	pdflatex -synctex=1 -interaction=nonstopmode $(MAIN_FILE).tex
	make clean

file-bibtex:
	pdflatex -interaction=errorstopmode $(MAIN_FILE).tex
	bibtex $(BIB_FILE)
	pdflatex -interaction=errorstopmode $(MAIN_FILE).tex
	pdflatex -interaction=errorstopmode $(MAIN_FILE).tex
	make clean

file-latexmk:
	latexmk
	make clean

clean:
	rm -rf *.aux
	rm -rf *.blg
	rm -rf *.dvi
	rm -rf *.fdb_latexmk
	rm -rf *.fls
	rm -rf *.gz
	rm -rf *.log
	rm -rf *.out
	rm -rf *.toc

clean-bbl:
	rm -rf *.bbl

clean-ax:
	rm -rf $(ARX_FILE)

preprint-ax:
	pdflatex $(MAIN_FILE).tex
	pdflatex $(MAIN_FILE).tex
	pdflatex $(MAIN_FILE).tex
	pdflatex $(MAIN_FILE).tex
	make clean
	make clean-ax
	tar -cvvf ax.tar --exclude=$(MAIN_FILE).pdf --exclude=$(ARX_FILE) --exclude=appendix --exclude=README.md --exclude=Makefile --exclude=LICENSE *
